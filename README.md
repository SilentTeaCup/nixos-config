My modular nixos configuration.
Different modules can be enabled/disabled on demand.

# Getting started

1. Clone the repository to /etc/nixos, you can also use a symlink
2. Then select the modules you want to use by creating a `modulesList.nix` you can use the example in `modulesList.nix.example`
3. Modify `configuration.nix` as you like

> **Note**: A `hardware-configuration.nix` is required for the configuration to build sucessfully.

# The module system

Each entry in `modulesList.nix` is basically a path inside `modules` specifying which files to activate.
For each module there can be a `<module-name>.nix` or `<module-name>.dot.nix` file (only one of them or both are fine).
The `*.dot.nix` files contain configurations that are applied via [home-manager](https://github.com/nix-community/home-manager) and are applied on a user basis.
They are only executed if `enableDotFiles` is set to true for a user.
The `*.nix` are system-wide configurations which are always applied if the module is selected.

# Building as iso image

```shell
nix-build '<nixpkgs/nixos>' -A config.system.build.isoImage -I nixos-config=iso.nix
```