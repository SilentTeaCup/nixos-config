{ config, lib, ... }:
with lib;
let
  cfg = config.boot.loader.grub.custom;
in
{

  options.boot.loader.grub.custom = {
    efiUuid = mkOption {
      type = types.str;
      description = "UUID of the efi partion used to find other bootloaders.";
      example = "1ce5-7f28";
      default = "";
    };
    windowsEntry = mkOption {
      type = types.bool;
      description = "Whether the grub menu should contain an entry for windows.";
      default = false;
    };
  };

  config = {

    boot.loader.systemd-boot.enable = false;
    boot.loader.efi.canTouchEfiVariables = true;
    boot.loader.grub = {
      device = "nodev";
      useOSProber = true;
      efiSupport = true;
      extraEntries = ''
        menuentry "Shutdown" {
          echo "Shutting down..."
          halt
        }
        menuentry "Reboot" {
          echo "Rebooting..."
          reboot
        }
        if [ ''${grub_platform} == "efi" ]; then
          menuentry "Frimware setup" {
            fwsetup
          }
        fi
        ${optionalString cfg.windowsEntry ''
          if [ ''${grub_platform} == "efi" ]; then
            menuentry "Windows" {
              savedefault
        search --no-floppy --set=root --file /EFI/Microsoft/Boot/bootmgfw.efi
              chainloader (\''${root})/EFI/Microsoft/Boot/bootmgfw.efi
            }
          fi 
        ''}
      '';
    };
  };

}

