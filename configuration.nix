{ ... }:

{

  # Import selected modules
  imports = [
    ((import ./modules.nix) {
      # Configure the enabled modules inside modulesList.nix
      # E.g. ["base" "locale"] for a minimal installation
      enabledModules = import ./modulesList.nix;
    })
    ./bootloader.nix
    ./hardware-configuration.nix
  ];

  nix.settings.auto-optimise-store = true;

  system.stateVersion = "22.11";

  system.autoUpgrade = {
    enable = true;
    allowReboot = true;
    channel = "https://nixos.org/channels/nixos-16.03-small/";
  };

  my.users.peter = {
    userName = "Peter Werner";
    email = "wpw.peter@gmail.com";
    enableDotFiles = true;
    isDev = true;
  };

  # Setup users
  users.users.peter = {
    isNormalUser = true;
    home = "/home/peter";
    uid = 1000;
    extraGroups = [ "wheel" "networkmanager" ];
  };

}
