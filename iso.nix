{ ... }:

{
  # Custom disk image for quicker installations/fixing

  imports = [
    <nixpkgs/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix>
    # Provide an initial copy of the NixOS channel so that the user
    # doesn't need to run "nix-channel --update" first.
    <nixpkgs/nixos/modules/installer/cd-dvd/channel.nix>
    ((import ./modules.nix) {
      enabledModules = [
        "base"
        "locale"
        "editor/nvim"
      ];
    })
  ];

}
