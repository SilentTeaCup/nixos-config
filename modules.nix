# Firt parameter defines modules to import
{ enabledModules, ... }: { config, pkgs, lib, ... }:
with lib;
let
  cfg = config.my;

  home-manager = fetchGit {
    url = "https://github.com/nix-community/home-manager.git";
    ref = "master";
    rev = "6bba64781e4b7c1f91a733583defbd3e46b49408";
  };

  asImports = { modules ? enabledModules, id ? "", dir ? ./modules }: filter
    (pathExists)
    (map (x: ./modules + "/${x}${id}.nix") modules);

  userModule = types.submodule {
    options = {
      userName = mkOption {
        type = types.str;
        example = "Steve Miller";
        description = ''
          Full name of the user.
        '';
      };
      email = mkOption {
        type = types.str;
        example = "steve@example.com";
        description = ''
          Email of the user.
        '';
      };
      enableDotFiles = mkOption {
        type = types.bool;
        default = true;
        description = ''
          Wether dotfiles should be deployed for this user.
        '';
      };
      isDev = mkOption {
        type = types.bool;
        default = false;
        description = ''
          Wether the user is a developer.
        '';
      };
    };
  };

  vscode-extensions =
    (import (fetchGit {
      url = "https://github.com/nix-community/nix-vscode-extensions";
      ref = "refs/heads/master";
      rev = "be62771c733f226651149c8a07c40c7c52c5ff5a";
    })).extensions.${builtins.currentSystem};

  tools = {
    pkgs.vscode-extensions = vscode-extensions;
  };

  allowUnfreePredicate = pkg: elem (lib.getName pkg) [
    "android-studio-stable"
  ];
in
{

  imports = [
    (import "${home-manager}/nixos")
  ]
  # Import host specific config for selected modules
  ++ asImports { dir = ./hosts + "/${config.networking.hostName}"; }
  # Import general config for selected modules
  ++ asImports { };

  options.my = {
    users = mkOption {
      type = with types; attrsOf userModule;
      description = "User configurations";
      default = { };
      example = {
        steve = {
          userName = "Steve Miller";
          email = "steve@example.com";
          enableDotFiles = false;
        };
      };
    };
  };

  config = {
    _module.args.tools = tools;

    home-manager.users = attrsets.mapAttrs'
      (n: user: attrsets.nameValuePair n {
        # Import dotfiles for selected modules
        imports = asImports { id = ".dot"; };
        home.stateVersion = "23.05";
        _module.args.user = user;
        _module.args.tools = tools;
        nixpkgs.config = {
          allowUnfreePredicate = allowUnfreePredicate;
        };
      })
      (filterAttrs (n: v: v.enableDotFiles) cfg.users);

    nixpkgs.config.allowUnfreePredicate = allowUnfreePredicate;
  };

}
