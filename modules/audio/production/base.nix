{ pkgs, ... }:

{

  environment.systemPackages =
    with pkgs;
    [
      lmms
      zyn-fusion
      qjackctl
    ];

}
