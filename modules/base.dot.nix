{ user, ... }:

{

  programs.git = {
    userName = user.userName;
    userEmail = user.email;
  };

  programs.ssh = {
    enable = true;
    extraConfig = ''
      AddKeysToAgent confirm
    '';
  };

  services.gpg-agent.enable = true;

}
