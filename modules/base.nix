{ pkgs, ... }:

{
  # Only keep the last 500MiB of systemd journal.
  services.journald.extraConfig = "SystemMaxUse=500M";

  # Common packages that are used on all systems.
  environment.systemPackages =
    with pkgs;
    [
      git
      gnupg
      # archives
      unzip
      p7zip
    ];

  environment.shellAliases = {
    "sys-upgrade" = "sudo nixos-rebuild switch --upgrade";
  };

  programs.ssh.startAgent = true;

}
