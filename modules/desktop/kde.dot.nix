{ ... }:

{

  imports = [
    ../../options/plasma/default.nix
  ];

  programs.plasma = {
    enable = true;
    theme = "breeze-dark";
    keyboard = {
      enable = true;
      layouts = [
        { id = "de"; }
      ];
    };
  };

  services.gpg-agent.pinentryFlavor = "qt";

}
