{ pkgs, ... }:

{

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the KDE Desktop Environment.
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;

  # Use NetworkManager
  networking.networkmanager.enable = true;

  # Standard set of applications
  environment.systemPackages =
    with pkgs;
    [
      # integration with smartphone
      kdeconnect
      # e-mail
      libsForQt5.kmail
      # calendar
      libsForQt5.korganizer
      # access calendar via console
      libsForQt5.akonadiconsole
      # pdf viewer
      libsForQt5.okular
      # image viewer
      gwenview
      # calculator
      qalculate-gtk
      # check disk space
      filelight
      # pinentry for gpg
      pinentry
      pinentry-qt
    ];

  programs.ssh.enableAskPassword = true;
  programs.ssh.askPassword = "${pkgs.libsForQt5.ksshaskpass}/bin/ksshaskpass";

  networking.firewall = {
    allowedTCPPortRanges = [
      # Required by kdeconnect
      { from = 1714; to = 1764; }
    ];
    allowedUDPPortRanges = [
      # Required by kdeconnect
      { from = 1714; to = 1764; }
    ];
  };

}
