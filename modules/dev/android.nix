{ config, pkgs, lib, ... }:
with lib;
{

  programs.adb.enable = true;

  users.users = attrsets.mapAttrs'
    (n: v: attrsets.nameValuePair n {
      extraGroups = [ "adbusers" ];
    })
    (filterAttrs (n: v: v.isDev) config.my.users);

  environment.systemPackages =
    with pkgs;
    [
      android-studio
    ];


}
