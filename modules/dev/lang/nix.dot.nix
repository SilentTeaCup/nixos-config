{ pkgs, ... }:

{

  programs.vscode.extensions = (with pkgs.vscode-extensions; [
    jnoortheen.nix-ide
  ]);
  programs.vscode.userSettings = {
    "nix.enableLanguageServer" = true;
    "nix.serverPath" = "nil";
    "nix.serverSettings" = {
      "nil" = {
        "formatting" = {
          "command" = [ "nixpkgs-fmt" ];
        };
      };
    };
  };

}
