{ tools, ... }:

{

  programs.vscode.extensions = (with tools.pkgs.vscode-extensions.open-vsx; [
    ms-python.python
    njpwerner.autodocstring
    # Formatting
    ms-python.flake8
    # Type checking
    ms-python.mypy-type-checker
    # Highlithing for pyproject.toml 
    tamasfe.even-better-toml
  ]);

  programs.vscode.userSettings = {
    # PyLance is not open source
    "python.languageServer" = "Jedi";
  };

}
