{ pkgs, lib, config, ... }:
with lib;
let
  cfg = config.my.python;
in
{

  options.my.python.packages = mkOption {
    type = types.listOf types.str;
    description = "Additional python packages";
    default = [ ];
    example = [ "conda" ];
  };

  config = {
    my.python.packages = [
      "poetry-core"
      "virtualenv"
    ];

    environment.systemPackages =
      with pkgs;
      let
        my-python-packages = python-packages: (
          map (pkg: python-packages."${pkg}") cfg.packages
        );
        python-with-my-packages = python3.withPackages my-python-packages;
      in
      [
        python-with-my-packages
        # package and dependency management
        pdm
      ];
  };

}
