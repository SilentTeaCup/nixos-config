{ pkgs, tools, ... }:

{

  programs.vscode = {
    enable = true;
    package = pkgs.vscodium;
    extensions = (with tools.pkgs.vscode-extensions.open-vsx; [
      redhat.vscode-yaml
      # Add eclipse keybindings
      alphabotsec.vscode-eclipse-keybindings
      # Sadyl this extension has no license
      # GuodongSun.vscode-git-cruise
      # Git diff, history and more
      eamodio.gitlens
    ]);
    mutableExtensionsDir = false;
    userSettings = {
      "editor.renderWhitespace" = "all";
      # Do not overwrite external askpass with integrated one
      "git.useIntegratedAskPass" = false;
      # Wrap at viewport or wordWrapColumn depending on what is smaller
      # This effectively removes the need for horizontal scrolling in any case
      # and also ensures that text is never to long to read efficiently
      "editor.wordWrap" = "bounded";
      # Generally used line width limits
      "editor.rulers" = [ 80 120 ];
      "editor.wordWrapColumn" = 80;
      # Disable redhat telementery form YAML language server
      "redhat.telemetry.enabled" = false;
      # Strip down gitlense since it conatins lots of obstrusive views
      "gitlens.plusFeatures.enabled" = false;
      "gitlens.showWelcomeOnInstall" = false;
      "gitlens.showWhatsNewAfterUpgrades" = false;
      "gitlens.telemetry.enabled" = false;
    };
    keybindings = [
      {
        "key" = "alt+f12";
        "command" = "workbench.action.terminal.toggleTerminal";
        "when" = "terminal.active";
      }
    ];
  };

}
