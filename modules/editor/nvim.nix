{ pkgs, ... }:

{

  # Common packages that are used on all systems.
  environment.systemPackages =
    with pkgs;
    [
      neovim
    ];

  environment.variables = {
    "EDITOR" = "nvim";
    "VISUAL" = "nvim";
  };

}
