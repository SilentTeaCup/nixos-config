{ ... }:

{

  # Time zone
  time.timeZone = "Europe/Berlin";
  # Languages
  i18n = {
    defaultLocale = "de_DE.UTF-8";
    supportedLocales = [
      "de_DE.UTF-8/UTF-8"
      "en_US.UTF-8/UTF-8"
      "en_GB.UTF-8/UTF-8"
    ];
  };

  # Keyboard
  console.keyMap = "de-latin1";

}
