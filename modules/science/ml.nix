{ pkgs, ... }:
let
  conda-wrapper = pkgs.writeScriptBin "conda" ''
    #!${pkgs.stdenv.shell}
    cmd='conda '"$@"
    conda-shell -c "$cmd"
  '';
in
{

  # Change to micromamba as soon as 
  # https://github.com/microsoft/vscode-python/issues/20919
  # is resolved as conda reqiures nix to emulate a standard file system
  # resulting in clunky usage 
  # (requires call to conda-shell to use most commands)
  environment.systemPackages =
    with pkgs;
    [
      conda
      conda-wrapper
    ];

}
