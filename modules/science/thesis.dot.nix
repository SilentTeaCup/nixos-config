{ ... }:

{

  imports = [
    ../typesetting/latex.dot.nix
  ];

  programs.chromium.extensions = [
    { id = "bmnfikjlonhkoojjfddnlbinkkapmldg"; } # bib it now!
  ];

}
