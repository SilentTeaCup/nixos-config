{ pkgs, ... }:

{

  imports = [
    ../typesetting/latex.nix
  ];

  my.latex.packages = [
    "collection-luatex"
    "acmart"
    "amsrefs"
    "appendix"
    "biber"
    "biblatex"
    "blindtext"
    "catchfile"
    "cfr-lm"
    "cleveref"
    "comment"
    "csquotes"
    "currfile"
    "datatool"
    "datetime2"
    "datetime2-german"
    "datetime2-english"
    "diagbox"
    "doctools"
    "filecontents"
    "floatflt"
    "framed"
    "fvextra"
    "glossaries"
    "glossaries-english"
    "glossaries-german"
    "glossaries-extra"
    "hyperxmp"
    "inconsolata"
    "ieeetran"
    "lccaps"
    "libertine"
    "lipsum"
    "llncs"
    "marginnote"
    "mdframed"
    "mfirstuc"
    "mindflow"
    "minted"
    "mnsymbol"
    "multirow"
    "mwe"
    "nag"
    "ncctools"
    "needspace"
    "newcomputermodern"
    "newtx"
    "newtxtt"
    "nfssext-cfr"
    "nowidow"
    "ntheorem"
    "pdfcomment"
    "preprint"
    "regexpatch"
    "scientific-thesis-cover"
    "snapshot"
    "soul"
    "soulpos"
    "sttools"
    "tcolorbox"
    "texlogsieve"
    "todonotes"
    "totpages"
    "tracklang"
    "xfor"
    "xpatch"
    "xstring"
    "yafoot"
    "zref"
  ];

  environment.systemPackages =
    with pkgs;
    [
      # Managing bib files
      jabref
    ];

}
