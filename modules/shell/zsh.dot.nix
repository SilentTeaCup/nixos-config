{ ... }:

{
  programs.zsh = {
    enable = true;
    dotDir = ".config/zsh";
    defaultKeymap = "emacs";
    plugins = [
      {
        name = "blue-lambda-theme";
        src = builtins.fetchGit {
          url = "https://gitlab.com/SilentTeaCup/blue-lambda.git";
          ref = "master";
          rev = "4a19a811b9c63c98b3bd81997d2b8ff8fead9dea";
        };
        file = "blue-lambda.zsh-theme";
      }
    ];
  };

  services.gpg-agent.enableZshIntegration = true;
}
