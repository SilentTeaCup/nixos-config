{ pkgs, config, lib, ... }:
with lib;
let
  cfg = config.my.latex;
in
{

  options.my.latex.packages = mkOption {
    type = types.listOf types.str;
    description = "Additional latex packages to include";
    default = [ ];
    example = [ "latexmk" ];
  };

  config = {
    my.latex.packages = [
      "scheme-small"
      "latexmk"
      "adjustbox"
      "appendixnumberbeamer"
      "bigints"
      "chngcntr"
      "enumitem"
      "collection-pictures"
      "collection-langenglish"
      "collection-langgerman"
      "collection-mathscience"
    ];

    environment.systemPackages =
      with pkgs;
      [
        (texlive.combine (attrsets.getAttrs cfg.packages texlive))
      ];
  };

}
