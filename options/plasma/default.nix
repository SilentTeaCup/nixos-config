{ config, lib, pkgs, ... }:

with lib;

let

  cfg = config.programs.plasma;

  common = {
    cfgDir =
      if pkgs.stdenv.hostPlatform.isDarwin then
        "Library/Application Support"
      else
        "${config.xdg.configHome}";
    renderBool = v: if v then "true" else "false";
  };

in
{

  imports = [
    ((import ./keyboard.nix) common)
  ];

  options = {
    programs.plasma = {
      enable = mkEnableOption "kde-plasma";
      theme = mkOption {
        type = types.str;
        default = "";
        example = literalExample "breeze-dark";
        description = ''
          The theme to use for kde plasma.
        '';
      };

    };
  };

  config = mkIf cfg.enable (mkMerge [
    {
      home.file."${common.cfgDir}/plasmarc".text = ''
        ${optionalString (cfg.theme != "") ''
          [Theme]
          name=${cfg.theme}
        ''}
      '';
    }
  ]);

}
