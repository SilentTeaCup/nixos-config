common: { config, lib, pkgs, ... }:

with lib;

let
  cfg = config.programs.plasma.keyboard;

  layoutModule = types.submodule {
    options = {
      id = mkOption {
        type = types.str;
        default = "en";
        example = literalExample "de";
        description = ''
          Id of the keyboard layout.
        '';
      };
      label = mkOption {
        type = types.str;
        default = "";
        example = literalExample "english";
        description = ''
          The displayed label for the layout.
        '';
      };
    };
  };

  indicatorModule = types.submodule {
    options = {
      show = mkOption {
        type = types.bool;
        default = true;
        description = ''
          Whether to show the layout indicator or not.
        '';
      };
      showFlag = mkOption {
        type = types.bool;
        default = false;
        description = ''
          Whether to show the layouts flag in the indicator.
        '';
      };
      showLabel = mkOption {
        type = types.bool;
        default = true;
        description = ''
          Whether to show the layouts label in the indicator.
        '';
      };
    };
  };
in
{

  options = {
    programs.plasma.keyboard = {
      enable = mkEnableOption "keyboard";
      layouts = mkOption {
        type = types.listOf layoutModule;
        default = [{ id = "us"; }];
        description = ''
          List of keyboard layouts to activate.
        '';
      };
      indicator = mkOption {
        type = indicatorModule;
        default = {
          show = true;
          showFlag = false;
          showLabel = true;
        };
        description = ''
          Settings for the layout indicator.
        '';
      };
      model = mkOption {
        type = types.str;
        default = "";
        example = "acer_laptop";
        description = ''
          Model of the keyboard.
        '';
      };
    };
  };

  config = mkIf cfg.enable (mkMerge [
    {
      home.file."${common.cfgDir}/kxkbrc".text =
        ''
          [Layout]
          DisplayNames=${concatStringsSep "," (
            map (x: x.label) cfg.layouts
          )}
          LayoutList=${concatStringsSep "," (
            map (x: x.id) cfg.layouts
          )}
          Model=${cfg.model}
          ResetOldOptions=false
          ShowFlag=${common.renderBool cfg.indicator.showFlag}
          ShowLabel=${common.renderBool cfg.indicator.showLabel}
          ShowLayoutIndicator=${common.renderBool cfg.indicator.show}
          ShowSingle=false
          SwitchMode=Global
          Use=true
        '';
    }
  ]);

}
